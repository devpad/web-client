// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

import axios from 'axios'
Object.defineProperty(Vue.prototype, '$axios', { value: axios })

import moment from 'moment'
Object.defineProperty(Vue.prototype, '$moment', { value: moment })

import fontawesome from '@fortawesome/fontawesome'
import { regular } from '@fortawesome/fontawesome-free-solid'

/* eslint-disable no-new */
new Vue({
	el: '#app',
	router,
	template: '<App/>',
	components: { App }
})
